#!/bin/bash

echo "Writing to local orange.yml"
echo "quantity: 1" > orange.yml
echo "Starting docker compose"
docker compose up -d
echo "Printing container's mounted orange.yml"
docker exec -it watch-orange-1 cat /home/orange.yml

echo "Updating local orange.yml"
echo "quantity: 5" > orange.yml
echo "Printing container's mounted orange.yml"
docker exec -it watch-orange-1 cat /home/orange.yml
echo "Stopping docker compose"
docker compose down
